﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace samolet
{
    public partial class zagruzka : Form
    {
        public zagruzka()
        {
            InitializeComponent();
            pictureBox1.Image = Properties.Resources.sam;
            progressBar1.Visible = false;
            progressBar1.Maximum = 2000000;
            
        }

        private void zagruzka_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            label2.Visible = false;
            progressBar1.Visible = true;
            pictureBox1.Image = null;
            pictureBox1.Visible = false;
            panel1.Visible = false;
            pictureBox2.Image = Properties.Resources.fon;
            pictureBox2.Refresh();            
            go();
        }
        public void go()
        {
            for (int i = 0; i < 2; i++)
                progressBar1.Increment(1);
            play player = new play();
            player.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = System.Drawing.Color.DeepSkyBlue;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = System.Drawing.Color.DeepSkyBlue;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.BackColor = System.Drawing.Color.DeepSkyBlue;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.BackColor = System.Drawing.Color.DeepSkyBlue;
        }
    }
}
