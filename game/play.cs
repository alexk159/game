﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SQLite;
using System.Diagnostics;

namespace samolet
{
    public partial class play : Form
    {
        int countone;
        int counttwo;
        int countone1;
        int counttwo1;
        int time;
        int budzhet;
        bool l=false;
        KeyValuePair<int, int> timing = new KeyValuePair<int, int>();
        System.Windows.Forms.Timer timer;
        DateTime stopwatch=DateTime.Now;
        DateTime gameTime = new DateTime(2015, 1, 1);
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        DataTable dt4 = new DataTable();
        DataTable dt5 = new DataTable();
        List<int> idreisovgruz = new List<int>();
        List<int> idreisovpassazh = new List<int>();

        List<int> idsamoletovgruz = new List<int>();
        List<int> idsamoletovpassazh = new List<int>();
        String pathToDB = "Data Source=C:\\users\\akiryushkin\\Planes.db";

        public static play SelfRef
        {
            get;
            set;
        }

        public play()
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();
            String s = stopwatch.Ticks.ToString();
            Debug.WriteLine(s);

            SQLiteCommand com = new SQLiteCommand("SELECT GameTime FROM MyInfo WHERE mainAirport='Пермь'", sqlite);
            long time = (long) com.ExecuteScalar();

            if (time != 0)
            {
                stopwatch = new DateTime(time);
            }

          

            InitializeComponent();
            sekundomer(1000);
           
            SelfRef = this;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();
            String s = stopwatch.Ticks.ToString();
            Debug.WriteLine(s);
            
            SQLiteCommand com = new SQLiteCommand("UPDATE MyInfo SET gameTime='" + s + "' WHERE mainAirport='Пермь'", sqlite);
            com.ExecuteNonQuery();
            
            Application.Exit();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            sekundomer_stop();
            time = trackBar1.Value;
            switch (time+1)
            {
                case 1:
                    sekundomer(1000);
                    break;
                case 2:
                    sekundomer(500);
                    break;
                case 3:
                    sekundomer(200);
                    break;
                case 4:
                    sekundomer(100);
                    break;
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
           
        }
        public void sekundomer(int milisecond)
        {
            timer = new System.Windows.Forms.Timer();
            timer.Tick += new EventHandler(tickTimer);
            timer.Interval = milisecond;
            timer.Start();
        }
        public void sekundomer_stop()
        {
             timer.Stop();
        }

        private void tickTimer(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();

            DateTime stopwatch1 = stopwatch.AddMinutes(1);
            stopwatch = stopwatch1;
            label5.Text = String.Format("{0:dd.MM.yyyyHH:mm}", stopwatch1);

            SQLiteCommand com1 = new SQLiteCommand("SELECT budget FROM MyInfo WHERE mainAirport='Пермь'", sqlite);
            long budget = (long)com1.ExecuteScalar();

            label3.Text = budget.ToString();

            DataTable dt11 = new DataTable();
            SQLiteDataAdapter com2 = new SQLiteDataAdapter("SELECT id, idPlane, finishDate, destination FROM Schedule", sqlite);

            com2.Fill(dt11);

            for (int i = 0; i < dt11.Rows.Count; i++)
            {
                DataRow dr = dt5.Rows[i];
                DateTime dT = DateTime.Parse(dr["finishDate"].ToString());
                if (dT <= stopwatch)
                {

                    SQLiteCommand com3 = new SQLiteCommand("UPDATE Schedule SET status='Выполнен' WHERE id='" + dr["id"].ToString() + "'", sqlite);
                    com3.ExecuteNonQuery();

                    SQLiteCommand com4 = new SQLiteCommand("UPDATE MyPlanes SET status='Стоит' WHERE id='" + dr["idPlane"].ToString() + "'", sqlite);
                    com3.ExecuteNonQuery();

                    SQLiteCommand com5 = new SQLiteCommand("UPDATE MyPlanes SET currentLocation='" + dr["destination"].ToString() +  "' WHERE id='" + dr["idPlane"].ToString() + "'", sqlite);
                    com5.ExecuteNonQuery();
                 
                }

                if (stopwatch.Minute % 20 == 0)
                {
                    fillTables();
                }

                if (stopwatch.Minute % 59 == 1)
                {
                    fillFlights();
                }

               
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
                l = false;
                budzh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
                l = true;
                budzh();
        }
        public void budzh()
        {
            if (l)
            {
                label6.Visible = true;
                label7.Visible = true;
                label5.Visible = false;
                label6.Text = "-";
                label7.Text = "30.000$";
                label6.Refresh();
                label7.Refresh();
                panel3.BackColor = System.Drawing.Color.Red;
                panel3.Refresh();
                budzhet = int.Parse(label3.Text);
                for (int i = 0; i <= 30000; i += 10)
                {
                    label3.Text = (budzhet - i).ToString();
                    if (int.Parse(label3.Text) < 0)
                    {
                        break;
                    }
                    label3.Refresh();
                    
                }
                panel3.BackColor=System.Drawing.Color.LightSteelBlue;
                label5.Visible = true;
                label6.Visible = false;
                label7.Visible = false;
            }
            else
            {
                label6.Visible = true;
                label7.Visible = true;
                label5.Visible = false;
                label6.Text = "+";
                label7.Text = "50.000$";
                label6.Refresh();
                label7.Refresh();
                panel3.BackColor = System.Drawing.Color.Green;
                panel3.Refresh();
                budzhet = int.Parse(label3.Text);
                for (int i = 0; i <= 50000; i += 10)
                {
                    label3.Text = (budzhet + i).ToString();
                    label3.Refresh();
                }
                panel3.BackColor = System.Drawing.Color.LightSteelBlue;
                label5.Visible = true;
                label6.Visible = false;
                label7.Visible = false;
            }

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
         
        }
        public void proverka()
        {
         
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label3_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            proverka();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            proverka();
        }

     

        private void button5_Click(object sender, EventArgs e)
        {
            pokupka p = new pokupka(this);
            p.Show();
        }

        private void play_Load(object sender, EventArgs e)
        {
            fillTables();
            fillFlights();
        }
        public void updateListboxes()
        {
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();

            for (int i = 0; i < dt3.Rows.Count; i++)
            {
                DataRow dr = dt3.Rows[i];
                listBox5.Items.Add(dr["name"].ToString());
            }

            for (int i = 0; i < dt4.Rows.Count; i++)
            {
                DataRow dr = dt4.Rows[i];
                listBox4.Items.Add(dr["name"].ToString());
            }

        }

        public void fillSchedule()
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();
            listView5.Clear();
            dt5.Clear();
            idreisovgruz.Clear();
            idsamoletovgruz.Clear();
            idsamoletovpassazh.Clear();
            idreisovpassazh.Clear();

            SQLiteDataAdapter ada5 = new SQLiteDataAdapter("SELECT * FROM Schedule ORDER BY finishDate DESC", sqlite);

            ada5.Fill(dt5);
            listView5.View = View.Details;
            listView5.Columns.Add("№");
            listView5.Columns.Add("Откуда");
            listView5.Columns.Add("Куда");
            listView5.Columns.Add("Название самолета");
            listView5.Columns.Add("Дата вылета");
            listView5.Columns.Add("Дата прилета");
            listView5.Columns.Add("Статус");
            listView5.Columns.Add("id Сам");

            for (int i = 0; i < dt5.Rows.Count; i++)
            {
                DataRow dr = dt5.Rows[i];
                ListViewItem listitem5 = new ListViewItem(dr["id"].ToString());
                listitem5.SubItems.Add(dr["out"].ToString());
                listitem5.SubItems.Add(dr["destination"].ToString());
                listitem5.SubItems.Add(dr["plane"].ToString());
                listitem5.SubItems.Add(dr["flightDate"].ToString());
                listitem5.SubItems.Add(dr["finishDate"].ToString());
                listitem5.SubItems.Add(dr["status"].ToString());
                listitem5.SubItems.Add(dr["idPlane"].ToString());
                listView5.Items.Add(listitem5);
            }

        }

        void fillFlights()
        {
            dt1.Clear();
            dt2.Clear();

            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();

            SQLiteDataAdapter ada1 = new SQLiteDataAdapter("SELECT Flights.idFlight, Flights.startFrom, Flights.destination, Flights.routeDistance, Flights.forfeit, CargoFlights.minLoadingCapacity, CargoFlights.flightPayment FROM Flights, Cargoflights WHERE Flights.idFlight=CargoFlights.idFlight order by random() limit 15", sqlite);
            ada1.Fill(dt1);

            SQLiteDataAdapter ada2 = new SQLiteDataAdapter("SELECT Flights.idFlight, Flights.startFrom, Flights.destination, Flights.routeDistance, Flights.forfeit, PassengerFlights.regularity, PassengerFlights.popularHours, PassengerFlights.minCapacity, PassengerFlights.maxCapacity, PassengerFlights.ticketPrice FROM Flights, PassengerFlights WHERE Flights.idFlight=PassengerFlights.idFlight order by random() limit 15", sqlite);

            ada2.Fill(dt2);

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                DataRow dr = dt1.Rows[i];
                listBox1.Items.Add(dr["startFrom"].ToString() + "-" + dr["destination"].ToString());
                idreisovgruz.Add(int.Parse(dr["idFlight"].ToString()));
                ListViewItem listitem1 = new ListViewItem(dr["idFlight"].ToString());
                listitem1.SubItems.Add(dr["startFrom"].ToString());
                listitem1.SubItems.Add(dr["destination"].ToString());
                listitem1.SubItems.Add(dr["routeDistance"].ToString());
                listitem1.SubItems.Add(dr["forfeit"].ToString());
                listitem1.SubItems.Add(dr["minLoadingCapacity"].ToString());
                listitem1.SubItems.Add(dr["flightPayment"].ToString());
                //listView1.Items.Add(listitem1);
            }

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt2.Rows[i];
                listBox2.Items.Add(dr["startFrom"].ToString() + "-" + dr["destination"].ToString());
                idreisovpassazh.Add(int.Parse(dr["idFlight"].ToString()));
                ListViewItem listitem1 = new ListViewItem(dr["idFlight"].ToString());

                //listView1.Items.Add(listitem1);
            }

            updateListboxes();
        }
        public void fillTables()
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();

            //listView1.Clear();
            //listView2.Clear();
            //listView3.Clear();
            //listView4.Clear();
            listView5.Clear();
            dt1.Clear();
            dt2.Clear();
            dt3.Clear();
            dt4.Clear();
            dt5.Clear();
            idreisovgruz.Clear();
            idsamoletovgruz.Clear();
            idsamoletovpassazh.Clear();
            idreisovpassazh.Clear();

          
            //listView1.View = View.Details;
            //listView1.Columns.Add("id");
            //listView1.Columns.Add("Откуда");
            //listView1.Columns.Add("Куда");
            //listView1.Columns.Add("Дистанция");
            //listView1.Columns.Add("Штраф");
            //listView1.Columns.Add("Минимальная зарузочная вместимость");
            //listView1.Columns.Add("Цена рейса");


           
            //listView2.View = View.Details;
            //listView2.Columns.Add("id");
            //listView2.Columns.Add("Откуда");
            //listView2.Columns.Add("Куда");
            //listView2.Columns.Add("Дистанция");
            //listView2.Columns.Add("Штраф");
            //listView2.Columns.Add("Регулярность");
            //listView2.Columns.Add("Час пик");
            //listView2.Columns.Add("Мин. кол-во пассажиров");
            //listView2.Columns.Add("Макс. кол-во пассажиров");
            //listView2.Columns.Add("Цена за билет");


            SQLiteDataAdapter ada3 = new SQLiteDataAdapter("SELECT MyPlanes.id, Planes.idPlane,  Planes.name, MyPlanes.possessionType, MyPlanes.possessionPrice, MyPlanes.currentLocation, MyPlanes.status from Planes, MyPlanes, CargoPlanes where Planes.idPlane=CargoPlanes.idPlane and MyPlanes.idPlane=Planes.idPlane and MyPlanes.status='Стоит'", sqlite);
            
            ada3.Fill(dt3);
            //listView3.View = View.Details;
            //listView3.Columns.Add("id");
            //listView3.Columns.Add("planeId");
            //listView3.Columns.Add("Название");
            //listView3.Columns.Add("Тип покупки");
            //listView3.Columns.Add("Цена");
            //listView3.Columns.Add("Текущая локация");
            //listView3.Columns.Add("Статус");


            SQLiteDataAdapter ada4 = new SQLiteDataAdapter("SELECT MyPlanes.id, Planes.idPlane, Planes.name, MyPlanes.possessionType, MyPlanes.possessionPrice, MyPlanes.currentLocation, MyPlanes.status from Planes, MyPlanes, PassengerPlanes where Planes.idPlane=PassengerPlanes.idPlane and MyPlanes.idPlane=Planes.idPlane and MyPlanes.status='Стоит'", sqlite);
            
            ada4.Fill(dt4);
            //listView4.View = View.Details;
            //listView4.Columns.Add("id");
            //listView4.Columns.Add("planeId");
            //listView4.Columns.Add("Название");
            //listView4.Columns.Add("Тип покупки");
            //listView4.Columns.Add("Цена");
            //listView4.Columns.Add("Текущая локация");
            //listView4.Columns.Add("Статус");

            SQLiteDataAdapter ada5 = new SQLiteDataAdapter("SELECT * FROM Schedule ORDER BY finishDate DESC", sqlite);
            
            ada5.Fill(dt5);
            listView5.View = View.Details;
            listView5.Columns.Add("№");
            listView5.Columns.Add("Откуда");
            listView5.Columns.Add("Куда");
            listView5.Columns.Add("Название самолета");
            listView5.Columns.Add("Дата вылета");
            listView5.Columns.Add("Дата прилета");
            listView5.Columns.Add("Статус");
            listView5.Columns.Add("id Сам");
            



            for (int i = 0; i < dt5.Rows.Count; i++)
            {
                DataRow dr = dt5.Rows[i];
                ListViewItem listitem5 = new ListViewItem(dr["id"].ToString());
                listitem5.SubItems.Add(dr["out"].ToString());
                listitem5.SubItems.Add(dr["destination"].ToString());
                listitem5.SubItems.Add(dr["plane"].ToString());
                listitem5.SubItems.Add(dr["flightDate"].ToString());
                listitem5.SubItems.Add(dr["finishDate"].ToString());
                listitem5.SubItems.Add(dr["status"].ToString());
                listitem5.SubItems.Add(dr["idPlane"].ToString());
                listView5.Items.Add(listitem5);
            }

  



            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                DataRow dr = dt2.Rows[i];
                listBox2.Items.Add(dr["startFrom"].ToString() + "-" + dr["destination"].ToString());
                idreisovpassazh.Add(int.Parse(dr["idFlight"].ToString()));
                ListViewItem listitem2 = new ListViewItem(dr["idFlight"].ToString());
                listitem2.SubItems.Add(dr["startFrom"].ToString());
                listitem2.SubItems.Add(dr["destination"].ToString());
                listitem2.SubItems.Add(dr["routeDistance"].ToString());
                listitem2.SubItems.Add(dr["forfeit"].ToString());
                listitem2.SubItems.Add(dr["regularity"].ToString());
                listitem2.SubItems.Add(dr["popularHours"].ToString());
                listitem2.SubItems.Add(dr["minCapacity"].ToString());
                listitem2.SubItems.Add(dr["maxCapacity"].ToString());
                listitem2.SubItems.Add(dr["ticketPrice"].ToString());
                //listView2.Items.Add(listitem2);
            }

            for (int i = 0; i < dt3.Rows.Count; i++)
            {
                DataRow dr = dt3.Rows[i];
                listBox5.Items.Add(dr["name"].ToString());
                idsamoletovgruz.Add(int.Parse(dr["id"].ToString()));
                ListViewItem listitem3 = new ListViewItem(dr["id"].ToString());
                listitem3.SubItems.Add(dr["idPlane"].ToString());
                listitem3.SubItems.Add(dr["name"].ToString());
                listitem3.SubItems.Add(dr["possessionType"].ToString());
                listitem3.SubItems.Add(dr["possessionPrice"].ToString());
                listitem3.SubItems.Add(dr["currentLocation"].ToString());
                listitem3.SubItems.Add(dr["status"].ToString());
                //listView3.Items.Add(listitem3);
            }

            for (int i = 0; i < dt4.Rows.Count; i++)
            {
                DataRow dr = dt4.Rows[i];
                listBox4.Items.Add(dr["name"].ToString());
                idsamoletovpassazh.Add(int.Parse(dr["id"].ToString()));
                ListViewItem listitem4 = new ListViewItem(dr["id"].ToString());
                listitem4.SubItems.Add(dr["idPlane"].ToString());
                listitem4.SubItems.Add(dr["name"].ToString());
                listitem4.SubItems.Add(dr["possessionType"].ToString());
                listitem4.SubItems.Add(dr["possessionPrice"].ToString());
                listitem4.SubItems.Add(dr["currentLocation"].ToString());
                listitem4.SubItems.Add(dr["status"].ToString());
                //listView4.Items.Add(listitem4);
            }

 
            updateListboxes();
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void play_FormClosing(object sender, FormClosingEventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();
            String s = stopwatch.Ticks.ToString();
            Debug.WriteLine(s);

            SQLiteCommand com = new SQLiteCommand("UPDATE MyInfo SET gameTime='" + s + "' WHERE mainAirport='Пермь'", sqlite);
            com.ExecuteNonQuery();
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            countone = listBox1.SelectedIndex;
            if (countone > -1)
            {
                listBox2.ClearSelected();
                listBox3.Items.Clear();
               // int k = idreisovgruz[countone]-1;

                listBox3.Items.Add("Дистанция: " + dt1.Rows[countone][3]);
                listBox3.Items.Add("Штраф: " + dt1.Rows[countone][4]);
                listBox3.Items.Add("Минимальная зарузочная вместимость: " + dt1.Rows[countone][5]);
                listBox3.Items.Add("Цена рейса: " + dt1.Rows[countone][6]);
            }
        }

        private void listBox2_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            counttwo = listBox2.SelectedIndex;
            if (counttwo > -1)
            {
                listBox1.ClearSelected();
                listBox3.Items.Clear();
               // int k = idreisovpassazh[counttwo]-1;

                listBox3.Items.Add("Дистанция: " + dt2.Rows[counttwo][3]);
                listBox3.Items.Add("Штраф: " + dt2.Rows[counttwo][4]);
                listBox3.Items.Add("Регулярность: " + dt2.Rows[counttwo][5]);
                listBox3.Items.Add("Час пик: " + dt2.Rows[counttwo][6]);
                listBox3.Items.Add("Мин. кол-во пассажиров: " + dt2.Rows[counttwo][7]);
                listBox3.Items.Add("Макс. кол-во пассажиров: " + dt2.Rows[counttwo][8]);
                listBox3.Items.Add("Цена за билет: " + dt2.Rows[counttwo][9]);

            }
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            countone1 = listBox4.SelectedIndex;
            if (countone1 > -1)
            {
                listBox5.ClearSelected();
                listBox6.Items.Clear();

                listBox6.Items.Add("Тип покупки: " + dt4.Rows[countone1][3]);
                listBox6.Items.Add("Текущая локация: " + dt4.Rows[countone1][5]);
                listBox6.Items.Add("Статус: " + dt4.Rows[countone1][6]);
            }
        }

        private void listBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            counttwo1 = listBox5.SelectedIndex;
            if (counttwo1 > -1)
            {
                listBox4.ClearSelected();
                listBox6.Items.Clear();
                int k = idsamoletovgruz[counttwo1];

                listBox6.Items.Add("Тип покупки: " + dt3.Rows[counttwo1][3]);
                listBox6.Items.Add("Текущая локация: " + dt3.Rows[counttwo1][5]);
                listBox6.Items.Add("Статус: " + dt3.Rows[counttwo1][6]);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();

            String date = String.Format("{0:dd.MM.yyyy HH:mm}", stopwatch);
            String reisID = idreisovpassazh[listBox2.SelectedIndex].ToString();
            String planeID = idsamoletovpassazh[listBox4.SelectedIndex].ToString();

            SQLiteCommand com9 = new SQLiteCommand("SELECT routeDistance FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
            int distance = Convert.ToInt32(com9.ExecuteScalar());


            SQLiteCommand com10 = new SQLiteCommand("SELECT Planes.speed FROM Planes, MyPlanes WHERE MyPlanes.id='" + planeID + "' AND MyPlanes.idPlane=Planes.idPlane", sqlite);
            int speed = Convert.ToInt32(com10.ExecuteScalar());

            int hours = ((distance / speed) % 10 == 0) ? distance / speed : distance / speed + 1;

            DateTime dt = stopwatch.AddHours(hours);
            String date1 = String.Format("{0:dd.MM.yyyy HH:mm}", dt);

            SQLiteCommand com1 = new SQLiteCommand("SELECT startFrom FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
            String startFrom = (String)com1.ExecuteScalar();

            SQLiteCommand com2 = new SQLiteCommand("SELECT destination FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
            String destination = (String)com2.ExecuteScalar();

            SQLiteCommand com3 = new SQLiteCommand("SELECT Planes.name FROM Planes, MyPlanes WHERE MyPlanes.id='" + planeID + "' AND MyPlanes.idPlane=Planes.idPlane" , sqlite);
            String plane = (String)com3.ExecuteScalar();

            SQLiteCommand com4 = new SQLiteCommand("insert into Schedule(out,destination,plane,flightDate,status, idPlane, finishDate) values ('" + startFrom + "','" + destination + "','" + plane + "','" + date + "','В Полете','" + planeID + "','" + date1 + "')", sqlite);
            com4.ExecuteNonQuery();

            SQLiteCommand com5 = new SQLiteCommand("SELECT capacity FROM PassengerPlanes, MyPlanes WHERE MyPlanes.id='" + planeID + "' AND MyPlanes.idPlane=PassengerPlanes.idPlane", sqlite);
            int capacity = Convert.ToInt32(com5.ExecuteScalar());

            SQLiteCommand com6 = new SQLiteCommand("SELECT ticketPrice FROM PassengerFlights WHERE idFlight='" + reisID + "'", sqlite);
            int cost = Convert.ToInt32(com6.ExecuteScalar());

            int sum = Convert.ToInt32(cost) * Convert.ToInt32(capacity);
            Debug.WriteLine("idPlane:" + planeID + ", cost:" + sum + ", capacity: " + capacity + ", costBefore: " + cost);

            SQLiteCommand com7 = new SQLiteCommand("UPDATE MyInfo SET budget = budget + " + sum.ToString(), sqlite);
            com7.ExecuteNonQuery();

            SQLiteCommand com8 = new SQLiteCommand("UPDATE MyPlanes SET status='В полете' WHERE id='" + planeID + "'", sqlite);
            com8.ExecuteNonQuery();
                                     
            fillTables();

        }

        private void listView5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();

            String date = String.Format("{0:dd.MM.yyyy HH:mm}", stopwatch);
            String reisID = idreisovgruz[listBox1.SelectedIndex].ToString();
            String planeID = idsamoletovgruz[listBox5.SelectedIndex].ToString();

            SQLiteCommand com0 = new SQLiteCommand("SELECT MyPlanes.currentLocation FROM MyPlanes WHERE id='" + planeID + "'", sqlite);
            String loc = Convert.ToString(com0.ExecuteScalar());

            SQLiteCommand com00 = new SQLiteCommand("SELECT Flights.startFrom FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
            String loc1 = Convert.ToString(com00.ExecuteScalar());

            if (loc == loc1)
            {
                SQLiteCommand com9 = new SQLiteCommand("SELECT routeDistance FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
                int distance = Convert.ToInt32(com9.ExecuteScalar());


                SQLiteCommand com10 = new SQLiteCommand("SELECT Planes.speed FROM Planes, MyPlanes WHERE MyPlanes.id='" + planeID + "' AND MyPlanes.idPlane=Planes.idPlane", sqlite);
                int speed = Convert.ToInt32(com10.ExecuteScalar());

                int hours = ((distance / speed) % 10 == 0) ? distance / speed : distance / speed + 1;

                DateTime dt = stopwatch.AddHours(hours);
                String date1 = String.Format("{0:dd.MM.yyyy HH:mm}", dt);


                SQLiteCommand com1 = new SQLiteCommand("SELECT startFrom FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
                String startFrom = (String)com1.ExecuteScalar();

                SQLiteCommand com2 = new SQLiteCommand("SELECT destination FROM Flights WHERE idFlight='" + reisID + "'", sqlite);
                String destination = (String)com2.ExecuteScalar();

                SQLiteCommand com3 = new SQLiteCommand("SELECT Planes.name FROM Planes, MyPlanes WHERE MyPlanes.id='" + planeID + "' AND MyPlanes.idPlane=Planes.idPlane", sqlite);
                String plane = (String)com3.ExecuteScalar();

                SQLiteCommand com4 = new SQLiteCommand("insert into Schedule(out,destination,plane,flightDate,status, idPlane, finishDate) values ('" + startFrom + "','" + destination + "','" + plane + "','" + date + "','В Полете','" + planeID + "','" + date1 + "')", sqlite);
                com4.ExecuteNonQuery();

                SQLiteCommand com6 = new SQLiteCommand("SELECT flightPayment FROM CargoFlights WHERE idFlight='" + reisID + "'", sqlite);
                int cost = Convert.ToInt32(com6.ExecuteScalar());

                SQLiteCommand com7 = new SQLiteCommand("UPDATE MyInfo SET budget = budget + " + cost.ToString(), sqlite);
                com7.ExecuteNonQuery();

                SQLiteCommand com8 = new SQLiteCommand("UPDATE MyPlanes SET status='В полете' WHERE id='" + planeID + "'", sqlite);
                com8.ExecuteNonQuery();

         

                fillTables();
            }
            else
            {
                MessageBox.Show("Самолет находится в городе " + loc + ", а отправление рейса осуществляется из города " + loc1);
            }

        }
    }
}
