﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace samolet
{
    public partial class pokupka : Form
    {
        String pathToDB = "Data Source=C:\\users\\akiryushkin\\Planes.db";
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        int countone;
        int counttwo;
        int cenaarendi;
        int cenalizinga;
        int cenapokupki;

        public static pokupka SelfRef { get; set; }

        public pokupka(Form form)
        {
            SelfRef = this;
            InitializeComponent();
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void pokupka_Load(object sender, EventArgs e)
        {
            listBox1.DrawMode = DrawMode.OwnerDrawFixed;
            listBox1.DrawItem += new DrawItemEventHandler(listBox1_DrawItem);
            listBox2.DrawMode = DrawMode.OwnerDrawFixed;
            listBox2.DrawItem += new DrawItemEventHandler(listBox2_DrawItem);
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);

            sqlite.Open();

            //listView3.View = View.Details;

            //listView3.Columns.Add("id");
            //listView3.Columns.Add("Название");
            //listView3.Columns.Add("Скорость");
            //listView3.Columns.Add("Макс. дист.");
            //listView3.Columns.Add("Вместимость погрузки");
            //listView3.Columns.Add("Цена обслуживания");
            //listView3.Columns.Add("Цена аренды");
            //listView3.Columns.Add("Цена лизинга");
            //listView3.Columns.Add("Цена покупки");

            //listView2.View = View.Details;

            //listView2.Columns.Add("id");
            //listView2.Columns.Add("Название");
            //listView2.Columns.Add("Скорость");
            //listView2.Columns.Add("Макс. дист.");
            //listView2.Columns.Add("Пассажировместимость");
            //listView2.Columns.Add("Цена обслуживания");
            //listView2.Columns.Add("Цена аренды");
            //listView2.Columns.Add("Цена лизинга");
            //listView2.Columns.Add("Цена покупки");
            

            SQLiteDataAdapter ada1 = new SQLiteDataAdapter("SELECT Planes.idPlane, Planes.name, Planes.speed, Planes.maxDistance, PassengerPlanes.capacity, Planes.serviceCost, Planes.rentalPrice, Planes.leasing, Planes.purchasePrice  FROM Planes, PassengerPlanes where Planes.idPlane=PassengerPlanes.idPlane", sqlite);
            ada1.Fill(dt1);

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                DataRow dr1 = dt1.Rows[i];
                listBox1.Items.Add(dr1["name"].ToString());
                //ListViewItem listitem1 = new ListViewItem(dr1["idPlane"].ToString());
                //listitem1.SubItems.Add(dr1["name"].ToString());
                //listitem1.SubItems.Add(dr1["speed"].ToString());
                //listitem1.SubItems.Add(dr1["maxDistance"].ToString());
                //listitem1.SubItems.Add(dr1["capacity"].ToString());
                //listitem1.SubItems.Add(dr1["serviceCost"].ToString());
                //listitem1.SubItems.Add(dr1["rentalPrice"].ToString());
                //listitem1.SubItems.Add(dr1["leasing"].ToString());
                //listitem1.SubItems.Add(dr1["purchasePrice"].ToString());
                //listView2.Items.Add(listitem1);
            }

            SQLiteDataAdapter ada = new SQLiteDataAdapter("SELECT Planes.idPlane, Planes.name, Planes.speed, Planes.maxDistance, CargoPlanes.loadingCapacity, Planes.serviceCost, Planes.rentalPrice, Planes.leasing, Planes.purchasePrice FROM Planes, CargoPlanes where Planes.idPlane=CargoPlanes.idPlane", sqlite);
            ada.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                listBox2.Items.Add(dr["name"].ToString());
                //ListViewItem listitem = new ListViewItem(dr["idPlane"].ToString());
                //listitem.SubItems.Add(dr["name"].ToString());
                //listitem.SubItems.Add(dr["speed"].ToString());
                //listitem.SubItems.Add(dr["maxDistance"].ToString());
                //listitem.SubItems.Add(dr["loadingCapacity"].ToString());
                //listitem.SubItems.Add(dr["serviceCost"].ToString());
                //listitem.SubItems.Add(dr["rentalPrice"].ToString());
                //listitem.SubItems.Add(dr["leasing"].ToString());
                //listitem.SubItems.Add(dr["purchasePrice"].ToString());
                //listView3.Items.Add(listitem);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();
            int idishnik;
            if (listBox1.SelectedIndex > -1) idishnik = listBox1.SelectedIndex;
            else idishnik = listBox2.SelectedIndex+listBox1.Items.Count;

            String id = (idishnik+1).ToString();

            String cost = cenapokupki.ToString();

            SQLiteCommand com = new SQLiteCommand();
            com.Connection = sqlite;
            com.CommandText = "UPDATE MyInfo SET budget = budget - " + cost;
            com.ExecuteNonQuery();

            com.CommandText = "INSERT INTO MyPlanes(possessionType, possessionPrice, status, currentLocation, idPlane) VALUES ('Покупка', '" + cost + "', 'Стоит', 'Пермь','" + id + "')";
            com.ExecuteNonQuery();

            play.SelfRef.fillTables();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();

            int idishnik;
            if (listBox1.SelectedIndex > -1) idishnik = listBox1.SelectedIndex;
            else idishnik = listBox2.SelectedIndex + listBox1.Items.Count;

            String id = (idishnik + 1).ToString();

            String cost = cenaarendi.ToString();

            SQLiteCommand com = new SQLiteCommand();
            com.Connection = sqlite;
            com.CommandText = "UPDATE MyInfo SET budget = budget - " + cost;
            com.ExecuteNonQuery();

            com.CommandText = "INSERT INTO MyPlanes(possessionType, possessionPrice, status, currentLocation, idPlane) VALUES ('Аренда', '" + cost + "', 'Стоит', 'Пермь','" + id + "')";
            com.ExecuteNonQuery();

            play.SelfRef.fillTables();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection(pathToDB);
            sqlite.Open();

            int idishnik;
            if (listBox1.SelectedIndex > -1) idishnik = listBox1.SelectedIndex;
            else idishnik = listBox2.SelectedIndex + listBox1.Items.Count;

            String id = (idishnik + 1).ToString();

            String cost = cenalizinga.ToString();

            SQLiteCommand com = new SQLiteCommand();
            com.Connection = sqlite;
            com.CommandText = "UPDATE MyInfo SET budget = budget - " + cost;
            com.ExecuteNonQuery();

            com.CommandText = "INSERT INTO MyPlanes(possessionType, possessionPrice, status, currentLocation, idPlane) VALUES ('Лизинг', '" + cost + "', 'Стоит', 'Пермь','" + id + "')";
            com.ExecuteNonQuery();

            play.SelfRef.fillTables();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            countone = listBox1.SelectedIndex;
            if (countone > -1)
            {
                listBox2.ClearSelected();
                listBox3.Items.Clear();

                listBox3.Items.Add("Скорость (км/ч): "+dt1.Rows[countone][2]);
                listBox3.Items.Add("Макс. дистанция: " + dt1.Rows[countone][3]);
                listBox3.Items.Add("Пассажировместимость: " + dt1.Rows[countone][4]);
                listBox3.Items.Add("Цена обслуживания: " + dt1.Rows[countone][5]);
                listBox3.Items.Add("Цена аренды: " + dt1.Rows[countone][6]);
                listBox3.Items.Add("Цена лизинга: " + dt1.Rows[countone][7]);
                listBox3.Items.Add("Цена покупки: " + dt1.Rows[countone][8]);

                cenaarendi= int.Parse((dt1.Rows[countone][6]).ToString());
                cenalizinga = int.Parse((dt1.Rows[countone][7]).ToString());
                cenapokupki = int.Parse((dt1.Rows[countone][8]).ToString());
            }
        }

        private void listBox2_SelectedIndexChanged_1(object sender, EventArgs e)
         {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            counttwo = listBox2.SelectedIndex;
            if (counttwo > -1)
            {
                
                listBox1.ClearSelected();
                listBox3.Items.Clear();

                listBox3.Items.Add("Скорость (км/ч): " + dt.Rows[counttwo][2]);
                listBox3.Items.Add("Макс. дистанция: " + dt.Rows[counttwo][3]);
                listBox3.Items.Add("Вместимость погрузки: " + dt.Rows[counttwo][4]);
                listBox3.Items.Add("Цена обслуживания: " + dt.Rows[counttwo][5]);
                listBox3.Items.Add("Цена аренды: " + dt.Rows[counttwo][6]);
                listBox3.Items.Add("Цена лизинга: " + dt.Rows[counttwo][7]);
                listBox3.Items.Add("Цена покупки: " + dt.Rows[counttwo][8]);

                cenaarendi = int.Parse((dt.Rows[counttwo][6]).ToString());
                cenalizinga = int.Parse((dt.Rows[counttwo][7]).ToString());
                cenapokupki = int.Parse((dt.Rows[counttwo][8]).ToString());
            }
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e = new DrawItemEventArgs(e.Graphics,
                                          e.Font,
                                          e.Bounds,
                                          e.Index,
                                          e.State ^ DrawItemState.Selected,
                                          e.ForeColor,
                                          Color.DarkOrange);
            e.DrawBackground();
            e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, Brushes.White, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        private void listBox2_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e = new DrawItemEventArgs(e.Graphics,
                                          e.Font,
                                          e.Bounds,
                                          e.Index,
                                          e.State ^ DrawItemState.Selected,
                                          e.ForeColor,
                                          Color.DarkOrange);
            e.DrawBackground();
            e.Graphics.DrawString(listBox2.Items[e.Index].ToString(), e.Font, Brushes.White, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = System.Drawing.Color.DarkOrange;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = System.Drawing.Color.DarkOrange;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.BackColor = System.Drawing.Color.SpringGreen;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.BackColor = System.Drawing.Color.DarkOrange;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pokupka_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}

